#!/bin/bash

echo "Starting parallel demultiplexation on input reads from $1"

for run in ./$1/*; do (
	for pool1 in $run/*_R1.fastq.gz; do
		pool2=$(echo "$pool1" | sed "s/R1/R2/")
		run_bn=$(basename $run)
		pool_bn=$(basename $pool1 | cut -d'_' -f1)
		prefix="./demultiplexed/$run_bn.$pool_bn."
		cutadapt -e 0 --no-indels -g "^file:_data/fwd.fasta" -G "^file:_data/rev.fasta" -o "$prefix{name1}-{name2}.1.fastq.gz" -p "$prefix{name1}-{name2}.2.fastq.gz" $pool1 $pool2
	done
  touch demultiplexed_log/$(basename $run) # for snakemake to know directory has been processed
  ) &
done
wait
echo "Demultiplexation finished..."
