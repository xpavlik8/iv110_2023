import csv
from collections import defaultdict
from os import listdir
from os.path import isfile, join
import pandas as pd

OUTPUT_FILE = "qiime2_partial_export/0_export.csv"

BRACKEN_LOGS = "qiime2_partial_export/"
brackens = [f for f in listdir(BRACKEN_LOGS) if isfile(join(BRACKEN_LOGS, f)) and f.endswith(".bracken") and not f.endswith(".bracken_out")]


all_families = set()
data = defaultdict(list)

for filename in brackens:
    with open(f"{BRACKEN_LOGS}/{filename}", "r") as bracken:
        reader = csv.reader(bracken, delimiter='\t')
        for line in reader:
            if len(line) < 3:
                continue
            _sample = filename.split("_")[0]
            _coverage = float(line[0])
            _class = line[3]
            _name = line[5].strip()

            if _class != "F":
                continue

            all_families.add(_name)
            data[_sample].append((_name, _coverage))

print(f"... extracted total {len(all_families)} families from {len(brackens)} files...")

df = pd.DataFrame(columns=["sample", "healthy"] + list(all_families))

for filename in brackens:
    _sample = filename.split("_")[0]

    master_entry = {}
    master_entry["sample"] = _sample
    master_entry["healthy"] = 1 if "Healthy" in filename else 0
    for family in all_families:
        master_entry[family] = 0.0
    for family, coverage in data[_sample]:
        master_entry[family] = coverage / 100  # represents %, move it directly to 0-1 so no scaling is required
    m = pd.DataFrame(master_entry, index=[0])

    df = pd.concat([df, m], ignore_index=True)
df.to_csv(f"{OUTPUT_FILE}", index=False)