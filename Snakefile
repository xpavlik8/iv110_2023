import os

configfile: "pipeline_config.yaml"

if config['execute_ggpicrust2']:
    ggpicrust2_trgt = "ggpicrust/ec-errorbar.png", "ggpicrust/ec-heatmap.png", "ggpicrust/ec-pca.png", "ggpicrust/ko-errorbar.png", "ggpicrust/ko-heatmap.png", "ggpicrust/ko-nokegg-errorbar.png", "ggpicrust/ko-pca.png"
else:
    ggpicrust2_trgt = "ggpicrust/mock.txt"

rule all:  # have a look at dag.pdf to see what really happens here
    input:
        # target verify
        "verify.ok",

        # target demultiplex -> filter -> qiime2 import
        "qiime2/data.qza",

        # target metadata_filtered
        "_data/metadata_filtered.tsv",

        # target quality (fastqc -> multiqc)
        "quality_R1/multiqc_report.html", "quality_R2/multiqc_report.html",

        # target denoise_join -> denoise_stats (joining pair reads, trimming, implicit filtering by dada2)
        "qiime2/stats.qza", "qiime2/table.qza", "qiime2/rep-seqs.qza",
        "qiime2/stats.qzv", "qiime2/table.qzv", "qiime2/rep-seqs.qzv",

        # target taxonomy_sklearn_classify
        "qiime2/taxonomy.qza", "qiime2/taxonomy.qzv", "qiime2/taxa-bar-plots.qzv",

        # target taxonomy_vsearch_classify (alternative), slower and w/ (a bit) inferior results
        # "qiime2/taxonomy_vsearch.qza", "qiime2/vsearch_results.qza",

        # target taxonomy_collapse_filter
        "qiime2/table_filtered.qza", "qiime2/table_collapsed.qza", "qiime2/table_collapsed.qzv", "qiime2/rep-seqs_filtered.qza",

        # target taxonomy_visualize
        "qiime2/taxonomy-krona.qzv", "qiime2/taxonomy-filtered-krona.qzv",
        "qiime2/taxa-bar-plots_filtered.qzv", "qiime2/taxa-bar-plots_collapsed.qzv",

        # target phylogeny
        "qiime2/aligned-rep-seqs.qza", "qiime2/masked-aligned-rep-seqs.qza", "qiime2/unrooted-tree.qza", "qiime2/rooted-tree.qza",

        # target (alpha/beta) diversity and alpha rarefaction
        "qiime2/core-metrics-results/faith-pd-group-significance.qzv", "qiime2/core-metrics-results/evenness-group-significance.qzv",
        "qiime2/core-metrics-results/unweighted-unifrac-category-significance.qzv", "qiime2/core-metrics-results/weighted-unifrac-category-significance.qzv",
        "qiime2/alpha-rarefaction.qzv",

        # target ancom
        "qiime2/ancom.qzv", "qiime2/ancom_collapsed.qzv",

        # target importance (https://docs.qiime2.org/2023.9/tutorials/pd-mice/)
        "qiime2/sample-classifier-results/heatmap_100-features.qzv",

        # target picrust
        "qiime2/picrust2/pathway_abundance.qzv",

        # target picrust-separate-[healthy|carcinoma]
        "qiime2/picrust2_healthy/pathway_abundance.qzv", "qiime2/picrust2_healthy/ko_metagenome.qza", "qiime2/picrust2_healthy/ec_metagenome.qza",
        "qiime2/picrust2_carcinoma/pathway_abundance.qzv", "qiime2/picrust2_carcinoma/ko_metagenome.qza", "qiime2/picrust2_carcinoma/ec_metagenome.qza",

        # target ggpicrust2_prepare
        "qiime2/picrust2/ko_metagenome/feature-table.tsv", "qiime2/picrust2/ec_metagenome/feature-table.tsv",
        "qiime2/picrust2_healthy/ko_metagenome/feature-table.tsv", "qiime2/picrust2_healthy/ec_metagenome/feature-table.tsv",
        "qiime2/picrust2_carcinoma/ko_metagenome/feature-table.tsv", "qiime2/picrust2_carcinoma/ec_metagenome/feature-table.tsv",

        # target kraken2_build
        "KRAKEN2_DB/hash.k2d",

        # target bracken_build
        "KRAKEN2_DB/database.kraken",

        # target kraken_repseqs_export
        "qiime2/seqs_raw/dna-sequences.fasta", "qiime2/seqs_raw_healthy/dna-sequences.fasta", "qiime2/seqs_raw_carcinoma/dna-sequences.fasta",

        # target kraken2_classify
        "qiime2/seqs_raw/k2.output", "qiime2/seqs_raw/k2.report", "qiime2/seqs_raw/k2_min.report",
        "qiime2/seqs_raw_healthy/k2.output", "qiime2/seqs_raw_healthy/k2.report", "qiime2/seqs_raw_healthy/k2_min.report",
        "qiime2/seqs_raw_carcinoma/k2.output", "qiime2/seqs_raw_carcinoma/k2.report", "qiime2/seqs_raw_carcinoma/k2_min.report",

        # target bracken_classify
        "qiime2/seqs_raw/bracken.txt", "qiime2/seqs_raw_healthy/bracken.txt", "qiime2/seqs_raw_carcinoma/bracken.txt",

        # target kraken_bracken_krona
        "qiime2/seqs_raw/k2.html", "qiime2/seqs_raw/bracken.html",
        "qiime2/seqs_raw_healthy/k2.html", "qiime2/seqs_raw_healthy/bracken.html",
        "qiime2/seqs_raw_carcinoma/k2.html", "qiime2/seqs_raw_carcinoma/bracken.html",

        # target randomforests_data
        "qiime2_partial_export/0_export.csv",

        # target randomforest
        "qiime2_partial_export/0_features.png", "qiime2_partial_export/0_stats.txt", "qiime2_partial_export/0_confusion.png", "qiime2_partial_export/0_features.html",
        "qiime2_partial_export/0_downsampled_features.png", "qiime2_partial_export/0_downsampled_stats.txt", "qiime2_partial_export/0_downsampled_confusion.png", "qiime2_partial_export/0_downsampled_features.html",

        # target ggpicrust2
        expand("{f}", f=ggpicrust2_trgt)


################################################################################################################################
# Preprocessing, filtering and joining reads

# Verifies all the input files and scripts versions
# Please note that r/python packages do not want to be verified, as installing them might be only viable
# in specific environments (aka executing some steps of pipeline in external programs, eg. RStudio)
rule verify:
    input:
        "verify.dat"
    output:
        "verify.ok"
    params:
        fastqc_bin=config['fastqc_bin'],
        krona_bin=config['krona_import_bin'],
        r_bin=config['r_bin']
    run:
        # following commands MUST succeed for pipeline to work (and guarantee consistency/reproducibility)
        # skipping this step is really not recommended, things will break and will break fast
        # though, this is faster than snakemake checking and switching conda environments
        shell("python _scripts/verify.py --check && touch verify.ok")
        shell("cutadapt --help")
        shell("qiime tools import --help")
        shell("{params.fastqc_bin} --help")
        shell("qiime dada2 denoise-paired --help")
        shell("qiime feature-classifier classify-sklearn --help")
        shell("qiime krona collapse-and-plot --help")  # additional plugin
        shell("qiime phylogeny align-to-tree-mafft-fasttree --help")
        shell("qiime sample-classifier classify-samples --help")
        shell("qiime picrust2 full-pipeline --help") # additional plugin
        shell("kraken2-build --help")
        shell("kraken2 --help")
        shell("{params.krona_bin} --help")
        shell("{params.r_bin} --help")
        # R plugins are not checked
        # python packages are not checked


# Demultiplexation: taking in raw illumina reads and splitting them based on adapters
# be careful, this generates A LOT of data, and you can easily run out of space
rule demultiplex:
    input:
        expand("{dir}/", dir=config['source_sequences_dir'])
    output:
        "demultiplexed_log/run83", "demultiplexed_log/run89", "demultiplexed_log/run90", "demultiplexed_log/run92",
        "demultiplexed_log/run93", "demultiplexed_log/run95", "demultiplexed_log/run96"
        # successful demultiplexation is marked by presence of files in demultiplexed_log (demultiplexed/* will be deleted after filtering)
        # as it is useless after and takes hundreds of GBs
    params:
        dir=config['source_sequences_dir']
    run:
        shell("rm -rf demultiplexed_log && mkdir demultiplexed_log")
        shell("rm -rf demultiplexed && mkdir demultiplexed")
        shell("./_scripts/demultiplex.sh {params.dir}")


# Filtering takes in demultiplexed data and matches forward and reverse sequence files (having same adapter class)
# and generates qiime2 import manifest for those samples which are expected to be present given the metadata AND filter
rule filter:
    input:
        "demultiplexed_log/run83","demultiplexed_log/run89","demultiplexed_log/run90","demultiplexed_log/run92",
        "demultiplexed_log/run93","demultiplexed_log/run95","demultiplexed_log/run96", "_data/metadata.tsv"
        # we blindly believe demultiplexed/* exists, but cannot rely on it pipeline-wise (we delete it after this step)
    output:
        "qiime2/import_manifesto"
    params:
        filter_cats = config['filtering_trgt_categories']
    run:
        shell("rm -rf filtered && mkdir filtered")
        shell("rm -rf qiime2 && mkdir qiime2")
        shell('python ./_scripts/filter.py "{params.filter_cats}"')
        shell("./_scripts/unzip.sh")
        shell("rm -rf demomultiplexed")     # to save space


# Also filtering metadata, by the same approach as above
rule metadata_filtered:
    input:
        "_data/metadata.tsv"
    output:
        "_data/metadata_filtered.tsv"
    params:
        filter_cats = config['filtering_trgt_categories']
    shell:
        'cat _data/metadata.tsv | head -n 1 > _data/metadata_filtered.tsv && cat _data/metadata.tsv | grep -E "{params.filter_cats}" >> _data/metadata_filtered.tsv'


# Importing forward and reverse reads into qiime2
rule qiime2_import:
    input:
        "qiime2/import_manifesto"
    output:
        "qiime2/data.qza"
    shell:
        "qiime tools import --type 'SampleData[PairedEndSequencesWithQuality]' --input-path ./qiime2/import_manifesto --output-path ./qiime2/data.qza --input-format PairedEndFastqManifestPhred33V2"


# Generating quality reports for all forward/reverse sequence files, to later (manually) decide for sequence joining parameters
rule quality:
    input:
        "qiime2/import_manifesto"  # we need files to be unzipped, which happens during step filter
    output:
        "quality_R1/multiqc_report.html", "quality_R2/multiqc_report.html",
    params:
        fastqc_bin=config['fastqc_bin']
    run:
        shell("rm -rf quality_R1 && mkdir quality_R1")
        shell("rm -rf quality_R2 && mkdir quality_R2")
        shell("./_scripts/fastqc.sh '{params.fastqc_bin}' 'quality_R1' 'R1' & ./_scripts/fastqc.sh '{params.fastqc_bin}' 'quality_R2' 'R2' & wait")


# Filtering, joining reads and removing chimeras utilizing qiime2's dada2 plugin
rule denoise_join:
    input:
        "qiime2/data.qza"
    output:
        "qiime2/rep-seqs.qza", "qiime2/table.qza", "qiime2/stats.qza"
    params:
        trunc_fwd=config['denoise_truncate_fwd'],  # we do not need to add support for forward trimming, as illumina data tend to be good
        trunc_rev=config['denoise_truncate_rev']   #  can be added tho
    shell:
        "qiime dada2 denoise-paired --i-demultiplexed-seqs ./qiime2/data.qza --p-trunc-len-f {params.trunc_fwd} --p-trunc-len-r {params.trunc_rev} --o-representative-sequences ./qiime2/rep-seqs.qza --o-table ./qiime2/table.qza --o-denoising-stats ./qiime2/stats.qza --verbose"


# Basic statistics from joining paired reads and their filtering.
rule denoise_stats:
    input:
        "qiime2/stats.qza", "qiime2/table.qza", "qiime2/rep-seqs.qza", "_data/metadata_filtered.tsv"
    output:
        "qiime2/stats.qzv", "qiime2/table.qzv", "qiime2/rep-seqs.qzv"
    run:
        shell("qiime feature-table summarize --i-table ./qiime2/table.qza --o-visualization ./qiime2/table.qzv --m-sample-metadata-file _data/metadata_filtered.tsv --verbose")
        shell("qiime feature-table tabulate-seqs --i-data ./qiime2/rep-seqs.qza --o-visualization ./qiime2/rep-seqs.qzv --verbose")
        shell("qiime metadata tabulate --m-input-file ./qiime2/stats.qza --o-visualization ./qiime2/stats.qzv --verbose")


################################################################################################################################
# qiime2's taxonomic analysis

# Taxonomic classification on ALL reads utilizing qiime2's sklearn models
# does like to eat a lot of RAM, so be careful
rule taxonomy_sklearn_classify:
    input:
        "_data/silva_new.qza", "qiime2/rep-seqs.qza", "qiime2/table.qza", "_data/metadata_filtered.tsv"
    output:
        "qiime2/taxonomy.qza", "qiime2/taxonomy.qzv", "qiime2/taxa-bar-plots.qzv"
    run:
        shell("qiime feature-classifier classify-sklearn --i-classifier _data/silva_new.qza --i-reads qiime2/rep-seqs.qza --o-classification qiime2/taxonomy.qza --p-n-jobs 2 --verbose")
        shell("qiime metadata tabulate --m-input-file qiime2/taxonomy.qza --o-visualization qiime2/taxonomy.qzv --verbose")
        shell("qiime taxa barplot --i-table qiime2/table.qza --i-taxonomy qiime2/taxonomy.qza --m-metadata-file _data/metadata_filtered.tsv --o-visualization qiime2/taxa-bar-plots.qzv --verbose")


# Taxonomic classification on ALL reads utilizing qiime2's vsearch
# this one is slow as hell
rule taxonomy_vsearch_classify:
    input:
        "qiime2/rep-seqs.qza", "_data/silva-138-99-seqs.qza", "_data/silva-138-99-tax.qza"
    output:
        "qiime2/taxonomy_vsearch.qza", "qiime2/vsearch_results.qza"
    shell:
        "qiime feature-classifier classify-consensus-vsearch --i-query ./qiime2/rep-seqs.qza --i-reference-reads _data/silva-138-99-seqs.qza --i-reference-taxonomy _data/silva-138-99-tax.qza --o-classification qiime2/taxonomy_vsearch.qza --o-search-results qiime2/vsearch_results.qza --p-threads 3"


# After taxonomic analysis, we want to collapse the taxons (higher up in taxonomy, as 16S rRNA do not have that much information for species level)
# and also we want to filter out mitochondria and chloroplast (not part of the analysis) and eucaryota (because of 18S rRNA pollution)
rule taxonomy_collapse_filter:
    input:
        "qiime2/table.qza", "qiime2/rep-seqs.qza", "qiime2/taxonomy.qza"
    output:
        "qiime2/table_filtered.qza", "qiime2/table_collapsed.qza", "qiime2/table_collapsed.qzv", "qiime2/rep-seqs_filtered.qza"
    params:
        taxa_level=config['collapse_to_taxonomy_level']
    run:
        shell("qiime taxa filter-table --i-table ./qiime2/table.qza --i-taxonomy ./qiime2/taxonomy.qza --p-exclude mitochondria,chloroplast,eukaryota --o-filtered-table ./qiime2/table_filtered.qza --verbose")
        shell("qiime taxa collapse --i-table ./qiime2/table_filtered.qza --i-taxonomy ./qiime2/taxonomy.qza --p-level {params.taxa_level} --o-collapsed-table ./qiime2/table_collapsed.qza --verbose")
        shell("qiime feature-table summarize --i-table ./qiime2/table_collapsed.qza --o-visualization ./qiime2/table_collapsed.qzv --m-sample-metadata-file _data/metadata_filtered.tsv --verbose")

        # sequences can only be filtered, as after collapsing taxonomy, IDs in table are changed forever and will not match
        # not a problem though, many analyses do not require sequences to run
        shell("qiime feature-table filter-seqs --i-data ./qiime2/rep-seqs.qza --i-table ./qiime2/table_filtered.qza --o-filtered-data ./qiime2/rep-seqs_filtered.qza")

        # defensively filter by (already filtered) table in order to protect ID mapping
        #shell("qiime taxa filter-seqs --i-sequences ./qiime2/rep-seqs.qza --i-taxonomy ./qiime2/taxonomy.qza --p-exclude mitochondria,chloroplast,eukaryota --o-filtered-sequences ./qiime2/rep-seqs_filtered.qza --verbose")


# Run krona visualization for all qiime2's taxonomic steps so far (as above)
rule taxonomy_visualize:
    input:
        "qiime2/table.qza", "qiime2/table_filtered.qza", "qiime2/table_collapsed.qza", "qiime2/taxonomy.qza"
    output:
        "qiime2/taxonomy-krona.qzv", "qiime2/taxonomy-filtered-krona.qzv",
        "qiime2/taxa-bar-plots_filtered.qzv", "qiime2/taxa-bar-plots_collapsed.qzv"
    run:
        shell("qiime krona collapse-and-plot --i-table qiime2/table.qza --i-taxonomy qiime2/taxonomy.qza --o-krona-plot qiime2/taxonomy-krona.qzv")
        shell("qiime krona collapse-and-plot --i-table qiime2/table_filtered.qza --i-taxonomy qiime2/taxonomy.qza --o-krona-plot qiime2/taxonomy-filtered-krona.qzv")

        # already done
        # shell("qiime taxa barplot --i-table qiime2/table.qza --i-taxonomy qiime2/taxonomy.qza --m-metadata-file _data/metadata_filtered.tsv --o-visualization qiime2/taxa-bar-plots.qzv --verbose")

        shell("qiime taxa barplot --i-table qiime2/table_filtered.qza  --m-metadata-file _data/metadata_filtered.tsv --o-visualization qiime2/taxa-bar-plots_filtered.qzv --verbose")
        shell("qiime taxa barplot --i-table qiime2/table_collapsed.qza  --m-metadata-file _data/metadata_filtered.tsv --o-visualization qiime2/taxa-bar-plots_collapsed.qzv --verbose")

################################################################################################################################
# Phylogenic and diversity analyses

# Phylogenic analysis for later diversity analyses (we do not really want collapsed taxons here, statistics goes brrr)
rule phylogeny:
    input:
        "qiime2/rep-seqs_filtered.qza"
    output:
        "qiime2/aligned-rep-seqs.qza", "qiime2/masked-aligned-rep-seqs.qza", "qiime2/unrooted-tree.qza", "qiime2/rooted-tree.qza"
    shell:
        "qiime phylogeny align-to-tree-mafft-fasttree --i-sequences qiime2/rep-seqs_filtered.qza --o-alignment qiime2/aligned-rep-seqs.qza --o-masked-alignment qiime2/masked-aligned-rep-seqs.qza --o-tree qiime2/unrooted-tree.qza --o-rooted-tree qiime2/rooted-tree.qza"


# Diversity analysis for our input data (again, no collapsed taxons here, as this would do bad stuff to statistical methods)
rule diversity:
    input:
        "_data/metadata_filtered.tsv", "qiime2/rooted-tree.qza", "qiime2/table_filtered.qza"
    output:
        "qiime2/core-metrics-results/faith-pd-group-significance.qzv", "qiime2/core-metrics-results/evenness-group-significance.qzv",
        "qiime2/core-metrics-results/unweighted-unifrac-category-significance.qzv", "qiime2/core-metrics-results/weighted-unifrac-category-significance.qzv",
        "qiime2/alpha-rarefaction.qzv"
    params:
        depth=config['diversity_sampling_depth'],
        max_depth=config['diversity_max_depth']
    run:

        # basic diversity statistics
        shell("rm -rf ./qiime2/core-metrics-results")
        shell("qiime diversity core-metrics-phylogenetic --i-phylogeny ./qiime2/rooted-tree.qza --i-table ./qiime2/table_filtered.qza --p-sampling-depth {params.depth} --m-metadata-file _data/metadata_filtered.tsv --output-dir ./qiime2/core-metrics-results")

        # both alpha-diversity statistics (FD, evenness)
        shell("qiime diversity alpha-group-significance --i-alpha-diversity ./qiime2/core-metrics-results/faith_pd_vector.qza --m-metadata-file _data/metadata_filtered.tsv --o-visualization ./qiime2/core-metrics-results/faith-pd-group-significance.qzv")
        shell("qiime diversity alpha-group-significance --i-alpha-diversity ./qiime2/core-metrics-results/evenness_vector.qza --m-metadata-file _data/metadata_filtered.tsv --o-visualization ./qiime2/core-metrics-results/evenness-group-significance.qzv")

        # both beta-diversity statistics (weighted, unweighted)
        shell("qiime diversity beta-group-significance --i-distance-matrix ./qiime2/core-metrics-results/unweighted_unifrac_distance_matrix.qza --m-metadata-file _data/metadata_filtered.tsv --m-metadata-column SampleMaterial --o-visualization ./qiime2/core-metrics-results/unweighted-unifrac-category-significance.qzv --p-pairwise")
        shell("qiime diversity beta-group-significance --i-distance-matrix ./qiime2/core-metrics-results/weighted_unifrac_distance_matrix.qza --m-metadata-file _data/metadata_filtered.tsv --m-metadata-column SampleMaterial --o-visualization ./qiime2/core-metrics-results/weighted-unifrac-category-significance.qzv --p-pairwise")

        # lastly, alpha-rarefaction for our dataset
        shell("qiime diversity alpha-rarefaction --i-table qiime2/table_filtered.qza --i-phylogeny qiime2/rooted-tree.qza --p-max-depth {params.max_depth} --m-metadata-file _data/metadata_filtered.tsv --o-visualization qiime2/alpha-rarefaction.qzv --verbose")


# Run ANCOM statistics given our data
rule ancom_filtered:
    input:
        "qiime2/table_filtered.qza"
    output:
        "qiime2/ancom.qzv"
    run:
        shell("qiime composition add-pseudocount --i-table ./qiime2/table_filtered.qza --o-composition-table ./qiime2/table_comp.qza")
        shell("qiime composition ancom --i-table ./qiime2/table_comp.qza --m-metadata-file _data/metadata_filtered.tsv  --m-metadata-column SampleMaterial --o-visualization ./qiime2/ancom.qzv")
        shell("rm qiime2/table_comp.qza")

# ... takes a lot of time, multiprocess it
rule ancom_collapsed:
    input:
        "qiime2/table_collapsed.qza"
    output:
        "qiime2/ancom_collapsed.qzv"
    run:
        shell("qiime composition add-pseudocount --i-table ./qiime2/table_collapsed.qza --o-composition-table ./qiime2/table_comp_collapsed.qza")
        shell("qiime composition ancom --i-table ./qiime2/table_comp_collapsed.qza --m-metadata-file _data/metadata_filtered.tsv  --m-metadata-column SampleMaterial --o-visualization ./qiime2/ancom_collapsed.qzv")
        shell("rm qiime2/table_comp_collapsed.qza")

################################################################################################################################
# Metabolic pathway analysis

# Generate underlying data for further metabolic analysis (we cannot, again, use collapsed taxons here and have to account for that later)
rule picrust:
    input:
        "qiime2/table_filtered.qza", "qiime2/rep-seqs_filtered.qza"
    output:
        "qiime2/picrust2/pathway_abundance.qzv", "qiime2/picrust2/ko_metagenome.qza", "qiime2/picrust2/ec_metagenome.qza"
    params:
        depth = config['picrust_sampling_depth']
    run:
        shell("rm -rf ./qiime2/picrust2")
        shell("qiime picrust2 full-pipeline --i-table ./qiime2/table_filtered.qza --i-seq ./qiime2/rep-seqs_filtered.qza --output-dir ./qiime2/picrust2 --p-placement-tool epa-ng --p-threads 3 --p-hsp-method mp --p-max-nsti 2 --verbose --p-highly-verbose")
        shell("qiime feature-table summarize --i-table ./qiime2/picrust2/pathway_abundance.qza --o-visualization ./qiime2/picrust2/pathway_abundance.qzv")
        shell("qiime diversity core-metrics --i-table ./qiime2/picrust2/pathway_abundance.qza --p-sampling-depth {params.depth} --m-metadata-file _data/metadata_filtered.tsv --output-dir ./qiime2/picrust2/pathabun_core_metrics_out --p-n-jobs 1")


# Generate raw metabolic data for sub-group of SampleMaterial, aka healthy
rule picrust_separate_healthy:
    input:
        "qiime2/table_filtered.qza", "qiime2/rep-seqs_filtered.qza"
    output:
        "qiime2/picrust2_healthy/pathway_abundance.qzv", "qiime2/picrust2_healthy/ko_metagenome.qza", "qiime2/picrust2_healthy/ec_metagenome.qza"
    params:
        depth = config['picrust_sampling_depth']
    run:
        shell("qiime feature-table filter-samples --i-table ./qiime2/table_filtered.qza --m-metadata-file _data/metadata_filtered.tsv --p-where \"[SampleMaterial] IN ('HealthyEsophagus')\" --o-filtered-table ./qiime2/table_tmp.qza")
        shell("qiime feature-table filter-seqs --i-data ./qiime2/rep-seqs_filtered.qza --i-table ./qiime2/table_tmp.qza --o-filtered-data ./qiime2/seqs_tmp.qza")

        shell("rm -rf ./qiime2/picrust2_healthy")
        shell("qiime picrust2 full-pipeline --i-table ./qiime2/table_tmp.qza --i-seq ./qiime2/seqs_tmp.qza --output-dir ./qiime2/picrust2_healthy --p-placement-tool epa-ng --p-threads 3 --p-hsp-method mp --p-max-nsti 2 --verbose --p-highly-verbose")
        shell("qiime feature-table summarize --i-table ./qiime2/picrust2_healthy/pathway_abundance.qza --o-visualization ./qiime2/picrust2_healthy/pathway_abundance.qzv")
        shell("qiime diversity core-metrics --i-table ./qiime2/picrust2_healthy/pathway_abundance.qza --p-sampling-depth {params.depth} --m-metadata-file _data/metadata_filtered.tsv --output-dir ./qiime2/picrust2_healthy/pathabun_core_metrics_out --p-n-jobs 1")
        shell("rm ./qiime2/table_tmp.qza && rm ./qiime2/seqs_tmp.qza")


# Generate raw metabolic data for sub-group of SampleMaterial, aka w/ carcinoma
rule picrust_separate_carcinoma:
    input:
        "qiime2/table_filtered.qza", "qiime2/rep-seqs_filtered.qza"
    output:
        "qiime2/picrust2_carcinoma/pathway_abundance.qzv", "qiime2/picrust2_carcinoma/ko_metagenome.qza", "qiime2/picrust2_carcinoma/ec_metagenome.qza"
    params:
        depth = config['picrust_sampling_depth']
    run:
        shell("qiime feature-table filter-samples --i-table ./qiime2/table_filtered.qza --m-metadata-file _data/metadata_filtered.tsv --p-where \"[SampleMaterial] IN ('EsophagealCarcinoma')\" --o-filtered-table ./qiime2/table_tmp1.qza")
        shell("qiime feature-table filter-seqs --i-data ./qiime2/rep-seqs_filtered.qza --i-table ./qiime2/table_tmp1.qza --o-filtered-data ./qiime2/seqs_tmp1.qza")

        shell("rm -rf ./qiime2/picrust2_carcinoma")
        shell("qiime picrust2 full-pipeline --i-table ./qiime2/table_tmp1.qza --i-seq ./qiime2/seqs_tmp1.qza --output-dir ./qiime2/picrust2_carcinoma --p-placement-tool epa-ng --p-threads 3 --p-hsp-method mp --p-max-nsti 2 --verbose --p-highly-verbose")
        shell("qiime feature-table summarize --i-table ./qiime2/picrust2_carcinoma/pathway_abundance.qza --o-visualization ./qiime2/picrust2_carcinoma/pathway_abundance.qzv")
        shell("qiime diversity core-metrics --i-table ./qiime2/picrust2_carcinoma/pathway_abundance.qza --p-sampling-depth {params.depth} --m-metadata-file _data/metadata_filtered.tsv --output-dir ./qiime2/picrust2_carcinoma/pathabun_core_metrics_out --p-n-jobs 1")
        shell("rm ./qiime2/table_tmp1.qza && rm ./qiime2/seqs_tmp1.qza")  # tmp1 for multiprocessing the pipeline and no race conditions


# Prepare data to be inported into ggpicrust2 (r package)
rule ggpicrust2_prepare:
    input:
        "_data/metadata_filtered.tsv", "qiime2/picrust2/ko_metagenome.qza", "qiime2/picrust2/ec_metagenome.qza"
    output:
        "qiime2/picrust2/ko_metagenome/feature-table.tsv", "qiime2/picrust2/ec_metagenome/feature-table.tsv",
        "qiime2/picrust2_healthy/ko_metagenome/feature-table.tsv", "qiime2/picrust2_healthy/ec_metagenome/feature-table.tsv",
        "qiime2/picrust2_carcinoma/ko_metagenome/feature-table.tsv", "qiime2/picrust2_carcinoma/ec_metagenome/feature-table.tsv"
    run:
        # all
        shell("unzip qiime2/picrust2/ko_metagenome.qza -d qiime2/picrust2/ko_metagenome")
        shell("mv qiime2/picrust2/ko_metagenome/*/data/feature-table.biom qiime2/picrust2/ko_metagenome/")
        shell("biom convert -i qiime2/picrust2/ko_metagenome/feature-table.biom -o qiime2/picrust2/ko_metagenome/feature-table.tmp --to-tsv")
        shell("cat qiime2/picrust2/ko_metagenome/feature-table.tmp | tail -n +2  > qiime2/picrust2/ko_metagenome/feature-table.tsv")  # header

        shell("unzip qiime2/picrust2/ec_metagenome.qza -d qiime2/picrust2/ec_metagenome")
        shell("mv qiime2/picrust2/ec_metagenome/*/data/feature-table.biom qiime2/picrust2/ec_metagenome/")
        shell("biom convert -i qiime2/picrust2/ec_metagenome/feature-table.biom -o qiime2/picrust2/ec_metagenome/feature-table.tmp --to-tsv")
        shell("cat qiime2/picrust2/ec_metagenome/feature-table.tmp | tail -n +2  > qiime2/picrust2/ec_metagenome/feature-table.tsv")

        # healthy
        shell("unzip qiime2/picrust2_healthy/ko_metagenome.qza -d qiime2/picrust2_healthy/ko_metagenome")
        shell("mv qiime2/picrust2_healthy/ko_metagenome/*/data/feature-table.biom qiime2/picrust2_healthy/ko_metagenome/")
        shell("biom convert -i qiime2/picrust2_healthy/ko_metagenome/feature-table.biom -o qiime2/picrust2_healthy/ko_metagenome/feature-table.tmp --to-tsv")
        shell("cat qiime2/picrust2_healthy/ko_metagenome/feature-table.tmp | tail -n +2  > qiime2/picrust2_healthy/ko_metagenome/feature-table.tsv")  # header

        shell("unzip qiime2/picrust2_healthy/ec_metagenome.qza -d qiime2/picrust2_healthy/ec_metagenome")
        shell("mv qiime2/picrust2_healthy/ec_metagenome/*/data/feature-table.biom qiime2/picrust2_healthy/ec_metagenome/")
        shell("biom convert -i qiime2/picrust2_healthy/ec_metagenome/feature-table.biom -o qiime2/picrust2_healthy/ec_metagenome/feature-table.tmp --to-tsv")
        shell("cat qiime2/picrust2_healthy/ec_metagenome/feature-table.tmp | tail -n +2  > qiime2/picrust2_healthy/ec_metagenome/feature-table.tsv")

        # carcinoma
        shell("unzip qiime2/picrust2_carcinoma/ko_metagenome.qza -d qiime2/picrust2_carcinoma/ko_metagenome")
        shell("mv qiime2/picrust2_carcinoma/ko_metagenome/*/data/feature-table.biom qiime2/picrust2_carcinoma/ko_metagenome/")
        shell("biom convert -i qiime2/picrust2_carcinoma/ko_metagenome/feature-table.biom -o qiime2/picrust2_carcinoma/ko_metagenome/feature-table.tmp --to-tsv")
        shell("cat qiime2/picrust2_carcinoma/ko_metagenome/feature-table.tmp | tail -n +2  > qiime2/picrust2_carcinoma/ko_metagenome/feature-table.tsv")  # header

        shell("unzip qiime2/picrust2_carcinoma/ec_metagenome.qza -d qiime2/picrust2_carcinoma/ec_metagenome")
        shell("mv qiime2/picrust2_carcinoma/ec_metagenome/*/data/feature-table.biom qiime2/picrust2_carcinoma/ec_metagenome/")
        shell("biom convert -i qiime2/picrust2_carcinoma/ec_metagenome/feature-table.biom -o qiime2/picrust2_carcinoma/ec_metagenome/feature-table.tmp --to-tsv")
        shell("cat qiime2/picrust2_carcinoma/ec_metagenome/feature-table.tmp | tail -n +2  > qiime2/picrust2_carcinoma/ec_metagenome/feature-table.tsv")


# Run ggpicrust2 (either as RScript or RStudio or sth else)
rule ggpicrust2:
    input:
        "qiime2/picrust2/ko_metagenome/feature-table.tsv", "qiime2/picrust2/ec_metagenome/feature-table.tsv"
    output:
        "ggpicrust/ec-errorbar.png", "ggpicrust/ec-heatmap.png", "ggpicrust/ec-pca.png", "ggpicrust/ko-errorbar.png", "ggpicrust/ko-heatmap.png",
        "ggpicrust/ko-nokegg-errorbar.png", "ggpicrust/ko-pca.png"
    params:
        currdir = os.getcwd(),
        R_executable = config['r_bin']
    run:
        # shell("rm -rf ggpicrust")   # we really want to be able to run this manually and as the last step, just trust me
        shell("mkdir -p ggpicrust")
        shell("{params.R_executable} {params.currdir}/_scripts/ggpicrust2.R")

rule ggpicrust2_mock:
    input:
        "qiime2/picrust2/ko_metagenome/feature-table.tsv", "qiime2/picrust2/ec_metagenome/feature-table.tsv"
    output:
        "ggpicrust/mock.txt"
    run:
        shell("mkdir -p ggpicrust")
        shell("echo 'not run' > ggpicrust/mock.txt")

################################################################################################################################
# Side-step kraken bacteriome analysis (to verify qiime2 classifier)

# build silva 138_1 kraken database
rule kraken2_build:
    output:
        "KRAKEN2_DB/hash.k2d"
    run:
        shell("rm -rf KRAKEN2_DB")
        shell("./_scripts/kraken2build.sh 'KRAKEN2_DB' 1")


# extend the database for bracken analysis
rule bracken_build:
    input:
        "KRAKEN2_DB/hash.k2d"
    output:
        "KRAKEN2_DB/database.kraken"
    run:
        shell("bracken-build -d KRAKEN2_DB -t 16 ")


# Export sequences for kraken to analyse (no filtering, no collapsing, just all sequences we have)
rule kraken_repseqs_export:
    input:
        "qiime2/table.qza", "qiime2/rep-seqs.qza", "_data/metadata_filtered.tsv"
    output:
        "qiime2/seqs_raw/dna-sequences.fasta", "qiime2/seqs_raw_healthy/dna-sequences.fasta", "qiime2/seqs_raw_carcinoma/dna-sequences.fasta"
    run:
        shell("rm -rf ./qiime2/seqs_raw")  # -> kraken2 all
        shell("qiime tools export --input-path ./qiime2/rep-seqs.qza --output-path ./qiime2/seqs_raw")

        shell("rm -rf ./qiime2/seqs_raw_healthy")  # -> kraken2 healthy
        shell("qiime feature-table filter-samples --i-table ./qiime2/table.qza --m-metadata-file _data/metadata_filtered.tsv --p-where \"[SampleMaterial] IN ('HealthyEsophagus')\" --o-filtered-table ./qiime2/table_raw_healthy.qza")
        shell("qiime feature-table filter-seqs --i-data ./qiime2/rep-seqs.qza --i-table ./qiime2/table_raw_healthy.qza --o-filtered-data ./qiime2/rep-seqs_healthy.qza")
        shell("qiime tools export --input-path ./qiime2/rep-seqs_healthy.qza --output-path ./qiime2/seqs_raw_healthy")
        shell("rm qiime2/table_raw_healthy.qza && rm qiime2/rep-seqs_healthy.qza")

        shell("rm -rf ./qiime2/seqs_raw_carcinoma") # -> kraken2 w/ carcinoma
        shell("qiime feature-table filter-samples --i-table ./qiime2/table.qza --m-metadata-file _data/metadata_filtered.tsv --p-where \"[SampleMaterial] IN ('EsophagealCarcinoma')\" --o-filtered-table ./qiime2/table_raw_carcinoma.qza")
        shell("qiime feature-table filter-seqs --i-data ./qiime2/rep-seqs.qza --i-table ./qiime2/table_raw_carcinoma.qza --o-filtered-data ./qiime2/rep-seqs_carcinoma.qza")
        shell("qiime tools export --input-path ./qiime2/rep-seqs_carcinoma.qza --output-path ./qiime2/seqs_raw_carcinoma")
        shell("rm qiime2/table_raw_carcinoma.qza && rm qiime2/rep-seqs_carcinoma.qza")


# Analyse all sequences using kraken2
rule kraken2_classify:
    input:
        "qiime2/seqs_raw/dna-sequences.fasta", "qiime2/seqs_raw_healthy/dna-sequences.fasta", "qiime2/seqs_raw_carcinoma/dna-sequences.fasta"
    output:
        "qiime2/seqs_raw/k2.output", "qiime2/seqs_raw/k2.report", "qiime2/seqs_raw/k2_min.report",
        "qiime2/seqs_raw_healthy/k2.output", "qiime2/seqs_raw_healthy/k2.report", "qiime2/seqs_raw_healthy/k2_min.report",
        "qiime2/seqs_raw_carcinoma/k2.output", "qiime2/seqs_raw_carcinoma/k2.report", "qiime2/seqs_raw_carcinoma/k2_min.report"
    run:
        # w/o minimizer
        shell("kraken2 --db 'KRAKEN2_DB' --report ./qiime2/seqs_raw/k2.report --output ./qiime2/seqs_raw/k2.output ./qiime2/seqs_raw/dna-sequences.fasta")
        shell("kraken2 --db 'KRAKEN2_DB' --report ./qiime2/seqs_raw_healthy/k2.report --output ./qiime2/seqs_raw_healthy/k2.output ./qiime2/seqs_raw_healthy/dna-sequences.fasta")
        shell("kraken2 --db 'KRAKEN2_DB' --report ./qiime2/seqs_raw_carcinoma/k2.report --output ./qiime2/seqs_raw_carcinoma/k2.output ./qiime2/seqs_raw_carcinoma/dna-sequences.fasta")

        # w/ minimizer
        shell("kraken2 --db 'KRAKEN2_DB' --report ./qiime2/seqs_raw/k2_min.report --report-minimizer-data ./qiime2/seqs_raw/dna-sequences.fasta")
        shell("kraken2 --db 'KRAKEN2_DB' --report ./qiime2/seqs_raw_healthy/k2_min.report --report-minimizer-data ./qiime2/seqs_raw_healthy/dna-sequences.fasta")
        shell("kraken2 --db 'KRAKEN2_DB' --report ./qiime2/seqs_raw_carcinoma/k2_min.report --report-minimizer-data ./qiime2/seqs_raw_carcinoma/dna-sequences.fasta")


# Collapse kraken2 results to required level
rule bracken_classify:
    input:
        "qiime2/seqs_raw/k2.output", "qiime2/seqs_raw/k2.report",
        "qiime2/seqs_raw_healthy/k2.output", "qiime2/seqs_raw_healthy/k2.report",
        "qiime2/seqs_raw_carcinoma/k2.output", "qiime2/seqs_raw_carcinoma/k2.report"
    output:
        "qiime2/seqs_raw/bracken.txt", "qiime2/seqs_raw_healthy/bracken.txt", "qiime2/seqs_raw_carcinoma/bracken.txt"
    params:
        level=config['bracken_collapse_to']
    run:
        shell("bracken -d KRAKEN2_DB -i ./qiime2/seqs_raw/k2.report -o ./qiime2/seqs_raw/bracken.log -w ./qiime2/seqs_raw/bracken.txt -l {params.level}")
        shell("bracken -d KRAKEN2_DB -i ./qiime2/seqs_raw_healthy/k2.report -o ./qiime2/seqs_raw_healthy/bracken.log -w ./qiime2/seqs_raw_healthy/bracken.txt -l {params.level}")
        shell("bracken -d KRAKEN2_DB -i ./qiime2/seqs_raw_carcinoma/k2.report -o ./qiime2/seqs_raw_carcinoma/bracken.log -w ./qiime2/seqs_raw_carcinoma/bracken.txt -l {params.level}")


# Visualize kraken2/bracken analysis above using kronographs
rule kraken_bracken_krona:
    input:
        "qiime2/seqs_raw/k2.report", "qiime2/seqs_raw/bracken.txt",
        "qiime2/seqs_raw_healthy/k2.report", "qiime2/seqs_raw_healthy/bracken.txt",
        "qiime2/seqs_raw_carcinoma/k2.report", "qiime2/seqs_raw_carcinoma/bracken.txt"
    output:
        "qiime2/seqs_raw/k2.html", "qiime2/seqs_raw/bracken.html",
        "qiime2/seqs_raw_healthy/k2.html", "qiime2/seqs_raw_healthy/bracken.html",
        "qiime2/seqs_raw_carcinoma/k2.html", "qiime2/seqs_raw_carcinoma/bracken.html"
    params:
        krona_bin=config['krona_import_bin']
    run:
        shell("python ./_scripts/kreport2krona.py --intermediate-ranks -r qiime2/seqs_raw/k2.report -o qiime2/seqs_raw/k2.krona ")
        shell("{params.krona_bin} -o qiime2/seqs_raw/k2.html qiime2/seqs_raw/k2.krona")

        shell("python ./_scripts/kreport2krona.py --intermediate-ranks -r qiime2/seqs_raw/bracken.txt -o qiime2/seqs_raw/bracken.krona ")
        shell("{params.krona_bin} -o qiime2/seqs_raw/bracken.html qiime2/seqs_raw/bracken.krona")

        shell("python ./_scripts/kreport2krona.py --intermediate-ranks -r qiime2/seqs_raw_healthy/k2.report -o qiime2/seqs_raw_healthy/k2.krona ")
        shell("{params.krona_bin} -o qiime2/seqs_raw_healthy/k2.html qiime2/seqs_raw_healthy/k2.krona")

        shell("python ./_scripts/kreport2krona.py --intermediate-ranks -r qiime2/seqs_raw_healthy/bracken.txt -o qiime2/seqs_raw_healthy/bracken.krona ")
        shell("{params.krona_bin} -o qiime2/seqs_raw_healthy/bracken.html qiime2/seqs_raw_healthy/bracken.krona")

        shell("python ./_scripts/kreport2krona.py --intermediate-ranks -r qiime2/seqs_raw_carcinoma/k2.report -o qiime2/seqs_raw_carcinoma/k2.krona ")
        shell("{params.krona_bin} -o qiime2/seqs_raw_carcinoma/k2.html qiime2/seqs_raw_carcinoma/k2.krona")

        shell("python ./_scripts/kreport2krona.py --intermediate-ranks -r qiime2/seqs_raw_carcinoma/bracken.txt -o qiime2/seqs_raw_carcinoma/bracken.krona ")
        shell("{params.krona_bin} -o qiime2/seqs_raw_carcinoma/bracken.html qiime2/seqs_raw_carcinoma/bracken.krona")


################################################################################################################################
# Machine learning models, importance analysis

# Export all samples' sequences, classify them using kraken2 and collapse using bracken
rule randomforests_data:
    input:
        "qiime2/table.qza", "qiime2/rep-seqs.qza", "_data/metadata_filtered.tsv", "KRAKEN2_DB/database.kraken"
    output:
        "qiime2_partial_export/0_export.csv"
    params:
        bracken_collapse = config["bracken_collapse_to"]
    run:
        shell("rm -rf qiime2_partial_export && mkdir qiime2_partial_export")
        shell("python ./_scripts/classify_samples.py {params.bracken_collapse}")  # generates the scripts
        shell("./qiime2_partial_export/0_extract.sh")  # extracts samples' sequences
        shell("./qiime2_partial_export/1_classify.sh")  # analyses them
        shell("python ./_scripts/combine_samples.py")  # combines them and generates the output


# Trains sklearn's randomforests classifier and exports feature importances as well as sample trees
rule randomforests:
    input:
        "qiime2_partial_export/0_export.csv"
    output:
        "qiime2_partial_export/0_features.png", "qiime2_partial_export/0_stats.txt", "qiime2_partial_export/0_confusion.png", "qiime2_partial_export/0_features.html",
        "qiime2_partial_export/0_downsampled_features.png", "qiime2_partial_export/0_downsampled_stats.txt", "qiime2_partial_export/0_downsampled_confusion.png", "qiime2_partial_export/0_downsampled_features.html"
    run:
        shell("python ./_scripts/iv110_model_taxonomy.py")
        shell("python ./_scripts/iv110_model_taxonomy.py --downsample")


# Train qiime2's sklearn classifier for features
rule importance:
    input:
        "_data/metadata_filtered.tsv", "qiime2/table_filtered.qza"
    output:
        "qiime2/sample-classifier-results/heatmap_100-features.qzv"
    run:
        shell("rm -rf ./qiime2/sample-classifier-results")
        shell("qiime sample-classifier classify-samples --i-table ./qiime2/table_filtered.qza --m-metadata-file _data/metadata_filtered.tsv --m-metadata-column SampleMaterial --p-random-state 420 --p-n-jobs 1 --output-dir ./qiime2/sample-classifier-results/")
        shell("qiime sample-classifier heatmap --i-table ./qiime2/table_filtered.qza --i-importance ./qiime2/sample-classifier-results/feature_importance.qza --m-sample-metadata-file _data/metadata_filtered.tsv --m-sample-metadata-column SampleMaterial --p-group-samples --p-feature-count 100 --o-heatmap ./qiime2/sample-classifier-results/heatmap_100-features.qzv --o-filtered-table ./qiime2/sample-classifier-results/filtered-table_100-features.qza")
