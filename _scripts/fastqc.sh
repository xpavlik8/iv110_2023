#!/bin/bash
# $1 ~> fastqc binary, $2 ~> out directory, $3 ~> tag
for filename in filtered/*$3.fastq; do
	echo "$3 | processing $filename"
	"$1" $filename -o $2
done

cd $2
multiqc . --interactive
cd ..
