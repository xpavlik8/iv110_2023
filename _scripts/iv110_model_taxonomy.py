import os
import pandas as pd
import numpy as np
from sklearn.inspection import permutation_importance
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, f1_score, confusion_matrix, ConfusionMatrixDisplay
from sklearn.preprocessing import StandardScaler
from sklearn.tree import export_graphviz
from matplotlib import pyplot as plt
import eli5
from eli5.sklearn import PermutationImportance
from argparse import ArgumentParser
from sklearn.utils import resample

EXPORT_DIR = "qiime2_partial_export"
TAG = "0"


parser = ArgumentParser()
parser.add_argument('--downsample', action='store_true', help='Downsample healthy dataset')
args = parser.parse_args()
if args.downsample:
    TAG = "0_downsampled"


# Load dataframes
df = pd.read_csv('/COMPUTE/iv110/iv110_data/qiime2_partial_export/0_export.csv')
print(f"... loaded data, shape {df.shape}")

# Downsample if requested
if args.downsample:
    print("~~~ [DOWNSAMPLING] ~~~")
    healthy = df[df["healthy"] == 1]
    carcinoma = df[df["healthy"] == 0]
    print(f"Before Healthy: {healthy.size}, Carcinoma: {carcinoma.size}")
    healthy_downsample = resample(healthy,
                              replace=True,
                              n_samples=len(carcinoma),
                              random_state=420)
    print(f"After Healthy: {healthy_downsample.size}, Carcinoma: {carcinoma.size}")
    df = pd.concat([healthy_downsample, carcinoma])

# Define features (X) and target variable (y)
target = df['healthy']
features = df.drop(columns=['sample', 'healthy'])

# Train-Test Split
X_train, X_test, y_train, y_test = train_test_split(features, target, test_size=0.2, random_state=420)
X_names = np.array(list(features.columns))

# no standardization nor normalization is needed (features already in 0-1 as per combine_samples.py)

# Define the parameter grid to search
param_grid = {
    'n_estimators': [50, 100, 200],
    'max_depth': [None, 10, 20, 30],
    'min_samples_split': [2, 5, 10],
    'min_samples_leaf': [1, 2, 4]
}

# Create a RandomForestClassifier
model2 = RandomForestClassifier()

# Instantiate GridSearchCV
grid_search = GridSearchCV(model2, param_grid, cv=10, scoring='f1', n_jobs=10, verbose=True)

# Fit the grid search to the data
print("... searching hyperparameter space for the best model")
grid_search.fit(X_train, y_train)

# Use the best model for predictions
best_model = grid_search.best_estimator_
print("... predicting")
y_pred = best_model.predict(X_test)

# Evaluate the accuracy of the best model
print("... evaluating accuracy/f1")
accuracy = accuracy_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)
print(f'Accuracy: {accuracy * 100:.2f}% / 100%')
print(f'F1: {f1:.2f} / 1')

with open(f"{EXPORT_DIR}/{TAG}_stats.txt", "w") as f:
    f.write(f"accuracy: {accuracy * 100} %\n")
    f.write(f"f1: {f1}\n")

# Another scoring metric, so-called confusion matrix
print("... evaluating confusion matrix")
plt.rcParams.update({'figure.figsize': (20.0, 10.0)})
plt.rcParams.update({'font.size': 14})
cm = confusion_matrix(y_test, y_pred, labels=[0, 1])
disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=["carcinoma", "healthy"])
disp.plot()
plt.xlabel("Confusion matrix")
plt.savefig(f'{EXPORT_DIR}/{TAG}_confusion.png')

print("... exporting trees")
for i, tree in enumerate(best_model.estimators_[:20]):
    print(f"Exporting tree {i} / {len(best_model.estimators_)}")
    export_graphviz(tree,
                    feature_names=X_names,
                    filled=True,
                    rounded=True,
                    out_file=f"{EXPORT_DIR}/tree_{i}_{TAG}.dot")
    os.system(f'dot -Tpng {EXPORT_DIR}/tree_{i}_{TAG}.dot -o {EXPORT_DIR}/tree_{i}_{TAG}.png')

print("... calculating n' exporting feature importances")

# https://mljar.com/blog/feature-importance-in-random-forest/

plt.clf()
plt.rcParams.update({'figure.figsize': (20.0, 8.0)})
plt.rcParams.update({'font.size': 14})

perm_importance = permutation_importance(best_model, X_test, y_test, scoring='f1')

nonzero_indexes = []
for i, val in enumerate(perm_importance.importances_mean):
    if val != 0:
        nonzero_indexes.append(i)
print(nonzero_indexes)
nonzero_indexes = np.array(nonzero_indexes)

plt.barh(X_names[nonzero_indexes], perm_importance.importances_mean[nonzero_indexes])
plt.xlabel("Random Forest Feature Importance (permutation_importance, top 20)")
plt.savefig(f'{EXPORT_DIR}/{TAG}_features.png')

perm = PermutationImportance(best_model, random_state=420, scoring="f1").fit(X_test, y_test)
w = eli5.show_weights(perm, feature_names = features.columns.tolist(), top=None)
result = pd.read_html(w.data)[0]
print(result)

with open(f'{EXPORT_DIR}/{TAG}_features.html', "w") as f:
    f.write(w.data)