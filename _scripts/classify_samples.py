import csv
import os
import shutil
import subprocess

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("bracken_collapse_level", help="Bracken collapse level, one letter from 'D','P','C','O','F','G','S'")
args = parser.parse_args()

assert args.bracken_collapse_level in ['D','P','C','O','F','G','S'], "unknown collapse level"

PARALLEL = 10
METADATA_FILE = "_data/metadata_filtered.tsv"
OUTPUT_SCRIPT_EXTRACT = "qiime2_partial_export/0_extract.sh"
OUTPUT_SCRIPT_CLASSIFY = "qiime2_partial_export/1_classify.sh"

OUTPUT_DIRECTORY = "qiime2_partial_export"

curr_dir = os.getcwd()

with open(OUTPUT_SCRIPT_CLASSIFY, "w") as out_classify_sh:
    with open(OUTPUT_SCRIPT_EXTRACT, "w") as out_extract_sh:
        with open(METADATA_FILE, "r") as file_in:
            metadata = csv.reader(file_in, delimiter='\t')
            for i, entry in enumerate(metadata):
                if (entry[0] == 'ID'):
                    continue
                qiime2_id = entry[0]
                qiime2_cat = entry[1]
                print(f"[{i}] Processing {qiime2_id} | {qiime2_cat}")
                out_extract_sh.write(f"(\necho '[{i}] Extracting {qiime2_id}'\n")
                out_extract_sh.write(f"qiime feature-table filter-samples --i-table ./qiime2/table.qza --m-metadata-file _data/metadata_filtered.tsv --p-where \"[ID] IN ('{qiime2_id}')\" --o-filtered-table ./{OUTPUT_DIRECTORY}/tmp{i}_tbl.qza\n")
                out_extract_sh.write(f"qiime feature-table filter-seqs --i-data ./qiime2/rep-seqs.qza --i-table ./{OUTPUT_DIRECTORY}/tmp{i}_tbl.qza --o-filtered-data ./{OUTPUT_DIRECTORY}/tmp{i}_seq.qza\n")
                out_extract_sh.write(f"qiime tools export --input-path ./{OUTPUT_DIRECTORY}/tmp{i}_seq.qza --output-path ./{OUTPUT_DIRECTORY}/tmp{i}\n")
                out_extract_sh.write(f"mv ./{OUTPUT_DIRECTORY}/tmp{i}/dna-sequences.fasta ./{OUTPUT_DIRECTORY}/{qiime2_id}_{qiime2_cat}.fasta\n")
                out_extract_sh.write(f"rm -rf ./{OUTPUT_DIRECTORY}/tmp{i}*\n) ")
                out_extract_sh.write("& " if i % PARALLEL != 0 else "& wait\n")

                out_classify_sh.write(f"echo '[{i}] Classifying {qiime2_id}'\n")
                out_classify_sh.write(f"kraken2 --db 'KRAKEN2_DB' --report ./{OUTPUT_DIRECTORY}/{qiime2_id}_{qiime2_cat}.kraken ./{OUTPUT_DIRECTORY}/{qiime2_id}_{qiime2_cat}.fasta\n")
                out_classify_sh.write(f"bracken -d KRAKEN2_DB -i ./{OUTPUT_DIRECTORY}/{qiime2_id}_{qiime2_cat}.kraken -w ./{OUTPUT_DIRECTORY}/{qiime2_id}_{qiime2_cat}.bracken -o ./{OUTPUT_DIRECTORY}/{qiime2_id}_{qiime2_cat}.bracken_out -l {args.bracken_collapse_level}\n")
        out_extract_sh.write("wait\n")

os.system(f"chmod +x {OUTPUT_SCRIPT_EXTRACT}")
os.system(f"chmod +x {OUTPUT_SCRIPT_CLASSIFY}")
