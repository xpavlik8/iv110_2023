import csv
import os
import re
from os import listdir
from pathlib import Path
from collections import defaultdict
import shutil
from os.path import isfile, join

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("categories", help="SampleMaterial categories to be accepted, delimeted by '|'")
args = parser.parse_args()

ACCEPTED_CATEGORIES = args.categories.split('|')
print(f"Filtering for categories {ACCEPTED_CATEGORIES}")
assert len(ACCEPTED_CATEGORIES) > 0, "no categories to be filtered"

################################################################
# Read metadata to know which samples we accept

runid = str
poolid = str
primer = int
sampleid = str
recognized_samples = {}  # : dict[tuple[runid, poolid, primer], sampleid]

with open("_data/metadata.tsv") as fd:
    rd = csv.reader(fd, delimiter="\t", quotechar='"')
    for row in rd:
        sample_id = row[0]
        category = row[1]
        if category not in ACCEPTED_CATEGORIES:
            continue
        location = row[2]
        primers = re.findall(r'\d+', row[3])
        if not primers:
            continue
        primer_id = int(primers[0])
        run_id = row[4]
        pool_id = row[5]
        recognized_samples[(run_id, pool_id, primer_id)] = sample_id


################################################################
# Process all files from demultiplexing, keep only allowed ones

import_manifest: str = ""
CURR_DIR = os.getcwd()

allfiles = [f for f in listdir("demultiplexed") if isfile(join("demultiplexed", f))]
idx = 0
for filename in allfiles:
    if '.1.' not in filename:
        continue

    R1_file = filename
    R2_file = filename.replace('.1.', '.2.')

    if R2_file not in allfiles:
        continue

    # both files are present

    runid = R1_file.split('.')[0]
    poolid = R1_file.split('.')[1]
    fwd_primer = int(re.findall(r'\d+', R1_file.split('.')[2].split('-')[0])[0]) if R1_file.split('.')[2].split('-')[0] != 'unknown' else -1
    bwd_primer = int(re.findall(r'\d+', R1_file.split('.')[2].split('-')[1])[0]) if R1_file.split('.')[2].split('-')[1] != 'unknown' else -1

    if fwd_primer == -1 or bwd_primer == -1 or fwd_primer != bwd_primer: # primers must match
        continue

    if ( runid, poolid, fwd_primer ) not in recognized_samples:
        continue
    sample_id = recognized_samples[(runid, poolid, fwd_primer)]

    print( f'found pair {R1_file} - {R2_file}, {runid}, {poolid} {fwd_primer} {bwd_primer} ~> {sample_id}' )
    idx += 1

    Path(f"filtered/").mkdir(parents=True, exist_ok=True)
    shutil.copy(f"demultiplexed/{R1_file}", f"filtered/{runid}.{poolid}.{fwd_primer}.{bwd_primer}.{idx}_R1.fastq.gz")
    shutil.copy(f"demultiplexed/{R2_file}", f"filtered/{runid}.{poolid}.{fwd_primer}.{bwd_primer}.{idx}_R2.fastq.gz")
    import_manifest += f"{sample_id}\t{CURR_DIR}/filtered/{runid}.{poolid}.{fwd_primer}.{bwd_primer}.{idx}_R1.fastq\t{CURR_DIR}/filtered/{runid}.{poolid}.{fwd_primer}.{bwd_primer}.{idx}_R2.fastq\n"

print(f"Successfully filtered and kept {idx} pair reads")

print("writing manifests, please wait...")
with open(f"qiime2/import_manifesto", "w") as f:
    f.write("sample-id\tforward-absolute-filepath\treverse-absolute-filepath\n")
    f.write(import_manifest)
print("filtering finished")


