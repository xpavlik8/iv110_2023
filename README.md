# Metagenomic analysis pipeline
### IV110 Projekt z bioinformatiky I, fall '23

---

This pipeline generates all the necessary underlying data and analyses for the report and presentation of our project.
Written under snakemake, it automatically, reliably and, most importantly, reproducibly processes source Illumina
sequences data, transforming and analysing them in the process.

## 1000 miles birds-eye view, aka the pipeline steps 

![Pipeline description](pipeline.png)

## Used tools

For simplicity and learning sake, this pipeline (written under [Snakemake](https://snakemake.readthedocs.io/en/stable/)) is almost fully integrated into [qiime2's](https://qiime2.org/) environment.
The first step (blue), preprocessing, is done using [cutadapt](https://cutadapt.readthedocs.io/en/stable/) and qiime2's [DADA2](https://benjjneb.github.io/dada2/) plugin.
Following, taxonomic analysis (green) is carried out using:
 - qiime2's [Naive Bayes classifiers](https://docs.qiime2.org/2023.9/data-resources/) utilizing [SILVA 138_1](https://www.arb-silva.de/documentation/release-1381/) database.
 - kraken2 using SILVA 128_1 database, later processed using [bracken](https://ccb.jhu.edu/software/bracken/) and collapsed to L6 (genus).
The taxonomic analysis is later visualised using [Krona](https://bio.tools/krona).
Phylogenic analyses as well as diversity analyses (red), are carried out utilising integrated qiime-amplicon plugins.
Metabolic pathway analyses (purple), are carried out using [PICRUSt](https://picrust.github.io/picrust/), again as qiime2 plugin and then later
analysed using [ggpicrust2](https://github.com/cafferychen777/ggpicrust2)
Machine learning used to find interesting patterns in our taxonomy results utilise qiime2's integrated plugin as well as python's [scikit-learn](https://scikit-learn.org) library.

It is possible (and was once integrated/supported) to build a custom qiime2 classifier (taxonomic analysis) using [RESCRIPt](https://github.com/bokulich-lab/RESCRIPt),
but this step was omitted in the final release as it was replaced by a full-scale kraken2 analysis.

## Running the pipeline

> Please note, due to too old versions of required software, many steps of this pipeline cannot be run (practically can, but they will internally fail fast) on `metacentrum` or similar services
> without pulling all your hair while trying to import external packages. As a friendly suggestion, do not even attempt it. This pipeline was NOT built for cluster/remote execution,
> it was built to be executed on reasonably powerful computer.

> Also please note, setting up environment such that all steps of the pipeline work can be extremely tricky.
> Many packaged and dependencies used **have to** be installed manually and attempt to install them automatically will fail.
> Some packages can require quite some hackery in order to be compatible with your os.
> Lastly, this pipeline was tested under Linux 6.6.2-arch1-1 with latest packages avaliable on 11th of December 2023.
> Any other linux distro will work. Any other OS will most likely fail, the only question is during which step.

All this is to say that even though this pipeline is fully integrated and works as expected, it is using tools
that might be hard to setup. You may look through all the data it has generated (Gdrive ~45GB file), but getting
everything ready to run it might take way too much time.

The pipeline is fully functional and given a correctly set-up environment, it is fully sufficient to run (given N cores of your processor)
> $ conda activate qiime_amplicon_environment
>
> $ snakemake -cN

... and wait for 12-ish hours. The pipeline allows for fully reproducible (and consistent) results as well as being able to
change specific steps, propagating updates where needed.

### Obtaining source data
Given the project input files (~25GB), a fully-run pipeline produces over 400GB of data, which can be later reduced to ~60GB by deleting files which are not
necessary for later analyses. Due to this and other reasons, this pipeline **is NOT shipped** with the data necessary to run it. Please download the following:
 -  Illumina sequences data from [link](https://is.muni.cz/auth/el/fi/podzim2023/IV110/index.qwarp?prejit=11818341) as "./illumina" folder
 -  SILVA 138_1 non-weighted Naive Bayes classifier [Silva 138 99% OTUs full-length sequences](https://docs.qiime2.org/2023.9/data-resources/) as "_data/silva_new.qza"
 -  SILVA 138_1 weighted Naive Bayes classifier [Weighted Silva 138 99% OTUs full-length sequences](https://docs.qiime2.org/2023.9/data-resources/) as "_data/silva_new_wght.qza"
 -  SILVA 138_1 source source sequences, [Silva 138 SSURef NR99 full-length sequences](https://docs.qiime2.org/2023.9/data-resources/), as "_data/silva-138-99-seqs.qza"
 -  SILVA 138_1 source source taxonomy, [Silva 138 SSURef NR99 full-length taxonomy](https://docs.qiime2.org/2023.9/data-resources/), as "_data/silva-138-99-tax.qza"

Please refer to verify.dat (or README_illumina.txt) for precisely written necessary locations as well as hashes of input files.
The pipeline's first step, `verify` checks the presence of necessary files as well as their hashes and fails early if source data is missing, misplaced or corrupted.
Disabling or bypassing this step is strongly discouraged.

### Setting up the environment

The snakemake's pipeline is run under conda environment, set up according to [Natively installing QIIME 2](https://docs.qiime2.org/2023.9/install/native/).
This might get tricky and may require you to use full-scale conda (not miniconda alternatives) as well as do some hackery while installing necessary plugins denoted in Snakemake file (step `verify`).
Also, please note that the pipeline is written under Linux and without modifying the commands, it will most likely not execute under other operating systems.
As for the other required packages and their releases, this project utilises all the necessary software available from conda/pip/AUR on the newest versions available on the 1st of December, 2023.

### Starting the pipeline and hardware restrictions

Due to speeding up the process, everything that could be run using multiple processes utilises the chance. It may be possible the pipeline
is unexpectedly killed by the OS (SIGKILL, SIGXCPU and similar) due to requiring too many resources. Please refer Snakemake file to manually disable
multiprocessing and make commands run sequentially. In the heaviest parts of the analysis, the pipeline takes about 50GB of RAM and utilises all available cores of the processor.

To start the pipeline, just run the first two commands from this section. 