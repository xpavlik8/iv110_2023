import hashlib
import os
import sys
from os import listdir
from os.path import isfile, join
from typing import List
import argparse


def list_files_rec(path: str, out: List[str]):
    files = [join(path, f) for f in listdir(path) if os.path.isfile(join(path, f)) and f != "verify.dat"]
    dirs = [join(path, f) for f in listdir(path) if os.path.isdir(join(path, f))]
    out.extend(files)
    for dir in dirs:
        list_files_rec(dir, out)

def md5sum(path):
    BUF_SIZE = 65536
    md5 = hashlib.md5()
    with open(path, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            md5.update(data)
    return "{0}".format(md5.hexdigest())

def truncate(string: str, maxlen: int):
    if len(string) < maxlen:
        return string
    return string[:maxlen - 3] + "..."


parser = argparse.ArgumentParser(description='Pipeline source file verification tool')
parser.add_argument('--generate', action='store_true', help='Generate verify.dat')
parser.add_argument('--check', action='store_true', help='Check against verify.dat')
args = parser.parse_args()

if not (args.generate or args.check):
    print("ERROR: at least one of --generate or --check must be specified")
    exit(2)

PRESENT_FILES = []
cwd = os.getcwd()

print("Verifying pipeline file consistency...")
print(f"Current working directory: {cwd}\n")
print("listing all files from current directory... ", end="")
list_files_rec('.', PRESENT_FILES)
print(f"found {len(PRESENT_FILES)}")

if args.generate:
    PRESENT_FILES_HASHES = {}
    print("calculating hashes of all files, please wait...")

    for i, path in enumerate(PRESENT_FILES):
        print(f"[ {round(i / len(PRESENT_FILES) * 100, 2)}% ] hashing {truncate(path, 60)}" + " " * 20, end="\r")
        sys.stdout.flush()
        PRESENT_FILES_HASHES[path] = md5sum(path)
    print("hashes calculated successfully...\n")

    print("Generating verify.dat file (manual curation will be necessary)")
    with open("verify.dat", 'w') as f:
        for rel_path, hash in PRESENT_FILES_HASHES.items():
            f.write(f"{rel_path}\t{hash}\n")
    print("... successfully finished!")
    exit(0)

if args.check:
    print("verifying expected files...")
    FILES_TO_CHECK = {}
    with open("verify.dat", "r") as f:
        for line in f:
            FILES_TO_CHECK[line.split('\t')[0].strip()] = line.split('\t')[1].strip()

    for i, path in enumerate(FILES_TO_CHECK):
        print(f"[ {round(i / len(FILES_TO_CHECK) * 100, 2)}% ] checking {truncate(path, 60)}" + " " * 20, end="\r")
        sys.stdout.flush()

        if not os.path.exists(path):
            print(f"\n\nERROR: while checking {path}: not present from current relative directory")
            exit(2)

        if not os.path.isfile(path):
            print(f"\n\nERROR: while checking {path}: not a regular file")
            exit(2)

        hash = md5sum(path)

        if hash != FILES_TO_CHECK[path]:
            print(f"\n\nERROR: while checking {path}: hash differs! expected: '{FILES_TO_CHECK[path]}', got: '{hash}'")
            exit(2)
    print("consistency of input files verified successfully!")
    exit(0)
